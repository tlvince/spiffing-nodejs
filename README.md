# spiffing-nodejs

> A curated list of Node.js packages

See [Awesome Node.js](https://github.com/sindresorhus/awesome-nodejs) for the
community edition.

## Packages

### HTTP

* [simple-get](https://github.com/feross/simple-get): Simplest way to make http get requests

### Testing

* [tap](https://github.com/tapjs/node-tap): Test Anything Protocol tools for node

## License

[![CC0](http://i.creativecommons.org/p/zero/1.0/88x31.png)](http://creativecommons.org/publicdomain/zero/1.0/)

To the extent possible under law, [Tom Vincent](https://tlvince.com) has waived
all copyright and related or neighbouring rights to this work.
